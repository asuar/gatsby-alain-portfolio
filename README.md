<h1 align="center">
  Alain Suarez's Portfolio Website
</h1>

This is my portfolio website. It was built with Gatsby to showcase my skills and experience.

## Features :star2:

:star: Modern UI design\
:star: React Reveal Animations\
:star: Image optimization through Gatsby\
:star: Bootstrap styling

The portfolio can be viewed **[here.](https://alainsuarez.dev)**

## Technologies used 🛠️

- [Gatsby](https://www.gatsbyjs.org/) - Static Site Generator
- [React](https://es.reactjs.org/) - Front-End JavaScript library
- [Bootstrap 4](https://getbootstrap.com/docs/4.3/getting-started/introduction/) - Front-End UI library

## Try out my code 🚀

This code can be deployed directly on a service such as [Netlify](https://netlify.com). Here is how to install it locally:

### Prerequisites :clipboard:

[Git](https://git-scm.com)\
[Node.js](https://nodejs.org/en/download/) (which comes with [NPM](http://npmjs.com))\
[Gatsby CLI](https://www.gatsbyjs.org/docs/quick-start/)

### Setup :wrench:

From the command line, first clone gatsby-alain-portfolio:

```bash
# Clone this repository
$ git clone https://gitlab.com/asuar/gatsby-alain-portfolio

# Go into the repository
$ cd gatsby-alain-portfolio

# Remove current origin repository
$ git remote remove origin
```

Then install the dependencies with NPM/Yarn:

Using NPM:

```bash
# Install dependencies
$ npm install

# Start development server
$ npm run develop
```

Once the server has started, the site can be accessed at `http://localhost:8000/`

## License 📄

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
