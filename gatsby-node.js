const path = require("path")
const fs = require("fs-extra")

exports.sourceNodes = async ({
  actions,
  createNodeId,
  createContentDigest,
}) => {
  const { createNode } = actions

  const historyImageList = await fs.readJson(
    path.resolve(__dirname, "./src/data/HistoryItems.json")
  )
  historyImageList.forEach(historyItem => {
    const id = historyItem.id
    const imgPath = historyItem.imageSrc
    const { name, ext } = path.parse(imgPath) // get file name & extension
    const absolutePath = path.resolve(__dirname, imgPath)
    const data = {
      name,
      ext,
      absolutePath, // <-- required
      extension: ext.substring(1), // <-- required, remove the dot in `ext`
    }
    const imageNode = {
      ...data,
      id: createNodeId(`history-image-${id}`),
      children: [],
      internal: {
        type: "historyItemsJson",
        contentDigest: createContentDigest(data),
      },
    }
    createNode(imageNode)
  })

  const projectsImageList = await fs.readJson(
    path.resolve(__dirname, "./src/data/ProjectItems.json")
  )
  projectsImageList.forEach(projectItem => {
    const id = projectItem.id
    const imgPath = projectItem.imageSrc
    const { name, ext } = path.parse(imgPath) // get file name & extension
    const absolutePath = path.resolve(__dirname, imgPath)
    const data = {
      name,
      ext,
      absolutePath, // <-- required
      extension: ext.substring(1), // <-- required, remove the dot in `ext`
    }
    const imageNode = {
      ...data,
      id: createNodeId(`project-image-${id}`),
      children: [],
      internal: {
        type: "projectItemsJson",
        contentDigest: createContentDigest(data),
      },
    }
    createNode(imageNode)
  })
}
