import React from "react"
import { Container, Row, Col } from "react-bootstrap"
import Fade from "react-reveal"

const Contact = () => {
  const TEXT_DELAY = 800
  const FADE_DURATION = 1000
  const FADE_DISTANCE = "30px"

  return (
    <section id="contact">
      <Container className="contact-wrapper">
        <h2 className="contact-wrapper__text text-center">
          {"Looking to hire me?"}
        </h2>
        <Row className="contact-btn-container">
          <Col className="col-6">
            <Fade
              bottom
              duration={FADE_DURATION}
              delay={TEXT_DELAY}
              distance={FADE_DISTANCE}
            >
              <a
                target="blank"
                rel="noopener noreferrer"
                className="cta-btn cta-btn-contact"
                href={"mailto:alain.s.compsc@gmail.com"}
              >
                {"Email"}
              </a>
            </Fade>
          </Col>
          <Col className="col-6">
            <Fade
              bottom
              duration={FADE_DURATION}
              delay={TEXT_DELAY}
              distance={FADE_DISTANCE}
            >
              <a
                target="blank"
                rel="noopener noreferrer"
                className="cta-btn cta-btn-contact"
                href={
                  "https://drive.google.com/file/d/1ALRNhRZWsaODXAFNiM4IfBjKTx7Uz9OU/view"
                }
              >
                {"Resume"}
              </a>
            </Fade>
          </Col>
        </Row>
      </Container>
    </section>
  )
}

export default Contact
